//
// A basic set of time utilities that facilitates various micro services
// that make cyclops framework itself
//
// (C) Cyclops Labs 2019
//

package main

import (
	"fmt"
	"gitlab.com/cyclops-utilities/date-utils/cyclops_utils"
)

func main() {
	err, from, to := cyclops_utils.GetReportingPeriod("2019-10-01")
	if err != nil {
		fmt.Errorf("Error in processing input: %s", err.Error())
	} else {
		fmt.Printf("From: %s, To: %s\n", from, to);
	}
}