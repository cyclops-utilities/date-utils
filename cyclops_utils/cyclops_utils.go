//
// A basic set of time utilities that facilitates various micro services
// that make cyclops framework itself
//
// (C) Cyclops Labs 2019
//

package cyclops_utils

import (
	"time"
)

func GetReportingPeriod(inputdate string) (err error, from, to string) {
	err = nil
	layout := "2006-01-02"
	t, err := time.Parse(layout, inputdate)
	if err != nil {
		return
	}
	y, m, d := t.Date()

	valTo := t.AddDate(0, 0, -1)
	to = valTo.Format("2006-01-02")
	valFrom := time.Now()
	if d == 1 && (m == 1 || m == 4 || m == 7 || m == 10) {
		//subtracting 1 day
		valFrom = t.AddDate(0,-3,0)

	} else {
		if m >= 1 && m <= 3 {
			valFrom = time.Date(y, time.January, 1,0,0,0,0, time.UTC)
		} else if m >= 4 && m <= 6 {
			valFrom = time.Date(y, time.April, 1,0,0,0,0, time.UTC)
		} else if m >= 7 && m <= 9 {
			valFrom = time.Date(y, time.July, 1,0,0,0,0, time.UTC)
		} else if m >= 10 && m <= 12 {
			valFrom = time.Date(y, time.October, 1,0,0,0,0, time.UTC)
		}
	}
	from = valFrom.Format("2006-01-02")
	return
}